![Figure 1 ](./DisplacementShadersBigWhite.gif)
Top Left: Default sprite, a river junction; Top Right: Simplex Noise in R G and B colour channels; Bottom Left: mask for the water portion of the sprite; Bottom Right: The effects of the displacement filter using the noise and mask sprites
