love.conf = function(t)
   t.gammacorrect = false
   t.title, t.identity = "shader-test", "Shader-Test"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 16* 4 * 4 * 2
   t.window.height = 16 * 4 * 4 * 2
   t.window.vsync = true
   t.window.x = 0
   t.window.y = 0
   t.window.resizable = false
   t.version = "11.4"
end
