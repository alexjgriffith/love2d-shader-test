(local lg love.graphics)
(lg.setDefaultFilter :nearest :nearest 0)
(local mask-shader (love.graphics.newShader :mask.glsl))
(local water-shader (love.graphics.newShader :water.glsl))
(local water-tile (lg.newImage :water.png))
(local simplex-noise (require :simplex-noise-tile-generator))

(local water-mask (lg.newCanvas 64 64))
(lg.push)
(lg.setCanvas water-mask)
(lg.setShader mask-shader)
(lg.draw water-tile)
(lg.setShader)
(lg.setCanvas)
(lg.pop)

(local w 128)
(local noise (simplex-noise 4 3 w w))

(fn love.load [])

(var time 0)
(fn love.update [dt]
  (set time (+ time dt)))

(fn love.draw []
  (lg.push)
  (lg.scale 4)
  (lg.push)
  (lg.setShader)
  (lg.draw water-tile)
  (lg.translate 0 (* 16 4))
  (lg.setShader mask-shader)
  (lg.draw water-tile)
  (lg.pop)
  (lg.setShader)
  (lg.translate (* 16 4) 0)
  (lg.draw noise)
  (lg.translate 0 (* 16 4))
  (lg.setShader water-shader)
  (water-shader:send :sprite_width 64)
  (water-shader:send :noise_width w)
  (water-shader:send :mask water-mask)
  (water-shader:send :noise noise)
  (water-shader:send :time time)
  (lg.draw water-tile)  
  (lg.pop))
