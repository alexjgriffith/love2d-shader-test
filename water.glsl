// we are using glsl1 so we can use 2d canvas context
#pragma language glsl1

// default love2d verted shader
#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
// noise tile size this example assumes the noise is present in all
// three channels and is the same size as the image being drawn
// you can scale the noise accross the image by passing in its
// dimensions
uniform Image noise;
// The mask represents the area that can be modified. It can either
// be prebaked or computed based on pallet index.
uniform Image mask;
// The time since the begining of the game
uniform float time;
uniform float noise_width;
uniform float sprite_width;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
  float speed = 0.05 * (sprite_width / noise_width);
  float amp = 0.05;
  // shift the noise index by time. Fract returns the fractional portion
  // of the float to ensure its between 0 and 1
  vec2 noise_time_index = fract(texture_coords * (sprite_width / noise_width) + vec2(speed * time, speed * time));
  // The noise colour channels r and gwill form the base offset in x and y
  vec4 noisecolor = Texel(noise, noise_time_index);
  // We use the b colour channel for for some counterflow
  float xy = noisecolor.b * 0.7071;
  noisecolor.a-=xy;
  noisecolor.b-=xy;
  // The displacement is the texture_coords offset by the noisecolor
  // In this example the offset is bound between + amp and - amp
  vec2 displacement = texture_coords + (((amp * 2) * vec2(noisecolor)) - amp);
  // Index the texture_coords for the sprite being drawn. This is the default
  // pixel colour
  vec4 texturecolor = Texel(tex, texture_coords);
  // index the mask colour. The mask should be the same size as the sprite
  // being drawn. We don't want to draw areas outside the mask onto the
  // mask region either so we index where the displaced pixel will come
  // from
  vec4 maskcolor = Texel(mask, texture_coords);
  vec4 maskcolor2 = Texel(mask, displacement);
  if (maskcolor.r > 0 && maskcolor2.r > 0){
    // if both the source and destination pixels fall into the mask region
    // replace the default texturecolor with the new displaced texturecolor
    texturecolor = Texel(tex, displacement);
  }
  return texturecolor * color;
}
#endif
