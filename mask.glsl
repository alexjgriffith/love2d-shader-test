#pragma language glsl1

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
uniform Image noise;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
  vec4 texturecolor = Texel(tex, texture_coords);
  // 99 155 255
  // 91 110 225
  if ((floor(texturecolor.r * 255) == 99 &&
       floor(texturecolor.g * 255) == 155 &&
       floor(texturecolor.b * 255) == 255) ||
      (floor(texturecolor.r * 255) == 91 &&
       floor(texturecolor.g * 255) == 110 &&
       floor(texturecolor.b * 255) == 225)
      ){
    texturecolor= vec4(1.0, 1.0, 1.0, 1.0);
  }else{
    texturecolor= vec4(0.0, 0.0, 0.0, 0.0);
  }
  return texturecolor * color;
}
#endif
